import { Component } from '@angular/core';

@Component({
    selector: 'signup-component',
    template: `{{title}}`,
})

export class SignupComponent {
    title = 'Signup page';

}
