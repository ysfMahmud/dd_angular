import { Component } from '@angular/core';

@Component({
    selector: 'booking',
    template: `        
        <router-outlet></router-outlet>
    `,
})

export class BookingComponent {

}
