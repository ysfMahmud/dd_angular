<?php

$providers = \common\models\Provider::find()->all();

$this->registerJs('
        function getAllItemPrices() {
    
           var item_total = 0;  
            
           $(".item_price").each(function () {
                item_total += parseFloat($(this).html())
                
                if(isNaN(item_total))
                {
                    $("#taqbin_normal_total").html("RM 0");
                    $("#taqbin_cod_total").html("RM 0");
                }
                else
                {
                    $("#taqbin_normal_total").html("RM " + item_total.toFixed(2).toString());
                    $("#taqbin_cod_total").html("RM " + ((item_total + '.$providers[0]->cod.').toFixed(2)).toString());
                }
           });
            
        } 


        $(document).on("keyup change click",".item-weight, .item-quantity",function(){       
         
              var item_weight;
              var item_quantity;
              var item_price = $(this).closest(".item.rows.clear.m-t.b-a.p-t.p-b-sm.light.b-4x").find(".item_price");
              
              
              if($(this).prop("name") == "BookingPack[items][quantity][]")
              {
                    item_quantity = $(this);
                    item_weight = $(this).closest(".item.rows.clear.m-t.b-a.p-t.p-b-sm.light.b-4x").find(".item-weight");
              }
              else
              {
                    item_weight = $(this);
                    item_quantity = $(this).closest(".item.rows.clear.m-t.b-a.p-t.p-b-sm.light.b-4x").find(".item-quantity");
              }      
                
              if(item_weight.val() <= 3.0)
              {
                  var price = (7.15 * parseInt(item_quantity.val()))*1.06;
                  
                  item_price.html(parseFloat(price).toFixed(2) + "RM");
              }
              else{
                  var price = ((Math.ceil(item_weight.val() - 2.99) * 1.1 + 7.15) * parseInt(item_quantity.val()))*1.06;
                  
                  item_price.html(parseFloat(price).toFixed(2) + "RM");
              }
              
              if((item_weight.val() == "") && (item_quantity.val() == ""))
              {
                  item_price.html("");
              }
            
              getAllItemPrices();
        });
        
       
');