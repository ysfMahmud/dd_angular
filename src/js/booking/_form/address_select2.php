<?php
use yii\helpers\Url;

$this->registerJs('
    $("#country_select1").change(function(){
        $.ajax({
            url: "' . Url::to(['booking/get-states']) . '",
            data: {country : $("#country_select1").val()},
            success: function(data) {
                $("#state_select1").html("<option>Select state ...</option>");
                data = JSON.parse(data);
                for (i = 0; i < data.length; i++) {
                    $("#state_select1").append("<option value=\""+data[i].state+"\">"+data[i].state+"</option>");
                }
                $("#state_select1").prop("disabled",false);
    
                if(state1 != false)
                    $("#state_select1").val(state1).trigger("change");
            }
        });
    })
    
    $("#state_select1").change(function(){
        $.ajax({
            url: "' . Url::to(['booking/get-areas']) . '",
            data: {
                state : $("#state_select1").val(),
                country : $("#country_select1").val()
            },
            success: function(data) {
                $("#area_select1").html("<option>Select area ...</option>");
                data = JSON.parse(data);
                for (i = 0; i < data.length; i++) {
                    $("#area_select1").append("<option value=\""+data[i].area+"\">"+data[i].area+"</option>");
                }
                $("#area_select1").prop("disabled",false);
    
                if(area1 != false)
                    $("#area_select1").val(area1).trigger("change");
            }
        });
    })
    
    $("#area_select1").change(function(){
        $.ajax({
            url: "' . Url::to(['booking/get-postcode']) . '",
            data: {
                area : $("#area_select1").val(),
                state : $("#state_select1").val(),
            },
            success: function(data) {
                $("#bookingpack-sh_service_area_id").html("");
                data = JSON.parse(data);
                for (i = 0; i < data.length; i++) {
                    $("#bookingpack-sh_service_area_id").append("<option value=\""+data[i].postcode+"\">"+data[i].postcode+"</option>");
                }
                $("#bookingpack-sh_service_area_id").prop("disabled",false);
            }
        });
    })
    
    ///////////////////////
    
    $("#country_select2").change(function(){
        $.ajax({
            url: "' . Url::to(['booking/get-states']) . '",
            data: {country : $("#country_select2").val()},
            success: function(data) {
                $("#state_select2").html("<option>Select state ...</option>");
                data = JSON.parse(data);
                for (i = 0; i < data.length; i++) {
                    $("#state_select2").append("<option value=\""+data[i].state+"\">"+data[i].state+"</option>");
                }
                $("#state_select2").prop("disabled",false);
    
                if(state2 != false)
                    $("#state_select2").val(state2).trigger("change");
            }
        });
    })
    
    $("#state_select2").change(function(){
        $.ajax({
            url: "' . Url::to(['booking/get-areas']) . '",
            data: {
                state : $("#state_select2").val(),
                country : $("#country_select2").val()
            },
            success: function(data) {
                $("#area_select2").html("<option>Select area ...</option>");
                data = JSON.parse(data);
                for (i = 0; i < data.length; i++) {
                    $("#area_select2").append("<option value=\""+data[i].area+"\">"+data[i].area+"</option>");
                }
                $("#area_select2").prop("disabled",false);
    
                if(area2 != false)
                    $("#area_select2").val(area2).trigger("change");
            }
        });
    })
    
    $("#area_select2").change(function(){
        $.ajax({
            url: "' . Url::to(['booking/get-postcode']) . '",
            data: {
                area : $("#area_select2").val(),
                state : $("#state_select2").val(),
            },
            success: function(data) {
                $("#bookingpack-b_service_area_id").html("");
                data = JSON.parse(data);
                for (i = 0; i < data.length; i++) {
                    $("#bookingpack-b_service_area_id").append("<option value=\""+data[i].postcode+"\">"+data[i].postcode+"</option>");
                }
                $("#bookingpack-b_service_area_id").prop("disabled",false);
            }
        });
    })
');



