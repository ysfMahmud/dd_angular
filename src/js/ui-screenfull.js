(function ($) {
	"use strict";
	var host = window.location.protocol+'//'+window.location.host;
	// custom settings
	if(window.location.host == 'projects.ukuya.net' || window.location.host == 'localhost' || window.location.host == '127.0.0.1') {
		host = host + '/dd';
	}
	uiLoad.load(host+'/js/plugins/screenfull/dist/screenfull.min.js');
	$(document).on('click', '[ui-fullscreen]', function (e) {
		e.preventDefault();
		if (screenfull.enabled) {
		  screenfull.toggle();
		}
	});
})(jQuery);
